//
//  TableViewController.swift
//  LineCharts
//
//  Created by Tran Son on 11/20/18.
//  Copyright © 2018 Tran Son. All rights reserved.
//

import UIKit
import Charts
class TableViewController: UITableViewController {

    @IBOutlet weak var lineChart: LineChartView!
    
    var months: Array<String> = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    let rainfall: Array<Double> = [30, 40, 45, 50, 55, 60, 65, 35, 25, 70, 75, 80]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        ForecastLineChart(name: months, value: rainfall)
    }
    
    func ForecastLineChart(name: [String], value: [Double]) {
        var lineArray: [ChartDataEntry] = []
        for i in 0..<name.count {
            let data: ChartDataEntry = ChartDataEntry(x: Double(i), y: value[i])
            lineArray.append(data)
        }
        let lineDataSet: LineChartDataSet = LineChartDataSet(values: lineArray, label: "Rainfall")
        lineDataSet.setColor(#colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1))
        
        let lineData: LineChartData = LineChartData(dataSet: lineDataSet)
        lineChart.data = lineData
        lineChart.animate(xAxisDuration: 2, easingOption: .easeInBounce)
        lineChart.animate(yAxisDuration: 2, easingOption: .easeInBounce)
        
    }
}
