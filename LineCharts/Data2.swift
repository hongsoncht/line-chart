//
//  Data2.swift
//  LineCharts
//
//  Created by Tran Son on 11/21/18.
//  Copyright © 2018 Tran Son. All rights reserved.
//

import Foundation
import UIKit

class dulieu2 {
    var dataLC: [dulieu]
    init(dataLC: [dulieu]) {
        self.dataLC = dataLC
    }

}

func lineChartData() -> dulieu2 {
    var data = [dulieu]()
    
    data.append(dulieu(DataChart: [20, 25, 35, 30]))
    data.append(dulieu(DataChart: [30, 25, 20, 30]))
    data.append(dulieu(DataChart: [25, 35, 20, 15]))
    
    return dulieu2(dataLC: data)
}
