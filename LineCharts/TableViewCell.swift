//
//  TableViewCell.swift
//  LineCharts
//
//  Created by Tran Son on 11/20/18.
//  Copyright © 2018 Tran Son. All rights reserved.
//

import UIKit
import Charts

class TableViewCell: UITableViewCell {

    @IBOutlet weak var chart: UIView!
    @IBOutlet weak var mChart: LineChartView!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var provinceLabel: UILabel!
    @IBOutlet weak var dollarsLabel: UILabel!
    @IBOutlet weak var percentLabel: UILabel!
    
    var dataEntries: [ChartDataEntry] = []
    var percent: Double = 0
    
    func setChart(values: [Double],place: String, province: String, dollars: String, percent: Double) {
        self.percent = percent
        mChart.noDataText = "No data available!"
        for i in 0..<values.count {
            print("chart point : \(values[i])")
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        
        placeLabel.text = place
        provinceLabel.text = province
        dollarsLabel.text = dollars
        percentLabel.text = "\(percent)%"
        
        
        let line1 = LineChartDataSet(values: dataEntries, label: "Units Consumed")
        line1.colors = [NSUIColor.blue]
        line1.mode = .cubicBezier
        line1.cubicIntensity = 0.2
        
        let gradient = getGradientFilling()
        line1.fill = Fill.fillWithLinearGradient(gradient, angle: 90.0)
        line1.drawFilledEnabled = true
        
        let data = LineChartData()
        data.addDataSet(line1)
        mChart.data = data
        mChart.setScaleEnabled(false)
        mChart.animate(xAxisDuration: 1.5)
        mChart.drawGridBackgroundEnabled = false
        mChart.xAxis.drawAxisLineEnabled = false
        mChart.xAxis.drawGridLinesEnabled = false
        mChart.leftAxis.drawAxisLineEnabled = false
        mChart.leftAxis.drawGridLinesEnabled = false
        mChart.rightAxis.drawAxisLineEnabled = false
        mChart.rightAxis.drawGridLinesEnabled = false
        mChart.legend.enabled = false
        mChart.xAxis.enabled = false
        mChart.leftAxis.enabled = false
        mChart.rightAxis.enabled = false
        mChart.xAxis.drawLabelsEnabled = false
        
        chart.layer.cornerRadius = 12.0
        chart.clipsToBounds = true
    }
    
     func getGradientFilling() -> CGGradient {
        
        if percent > 0 {
            percentLabel.textColor = UIColor.blue
            let coloTop = UIColor(red: 10/255, green: 10/255, blue: 250/255, alpha: 1).cgColor
            let colorBottom = UIColor(red: 10/255, green: 10/255, blue: 250/255, alpha: 1).cgColor
            let gradientColors = [coloTop, colorBottom] as CFArray
            let colorLocations: [CGFloat] = [0.7, 0.0]
            return CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations)!
        }
        else {
            percentLabel.textColor = UIColor.red
            let coloTop = UIColor(red: 250/255, green: 10/255, blue: 10/255, alpha: 1).cgColor
            let colorBottom = UIColor(red: 250/255, green: 10/255, blue: 10/255, alpha: 1).cgColor
            let gradientColors = [coloTop, colorBottom] as CFArray
            let colorLocations: [CGFloat] = [0.7, 0.0]
            return CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations)!
        }
    }
}
