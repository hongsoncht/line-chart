//
//  ViewController.swift
//  LineCharts
//
//  Created by Tran Son on 11/20/18.
//  Copyright © 2018 Tran Son. All rights reserved.
//

import UIKit
import Charts

struct DataChart {
    var valueStock: [Double]
}
class ViewController: UITableViewController {
    
    var dataStock = [DataChart]()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataStock = [DataChart.init(valueStock: [20, 25, 35, 30]), DataChart.init(valueStock: [30, 25, 20, 30]), DataChart.init(valueStock: [25, 35, 20, 15])]
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataStock.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Segue", for: indexPath) as! TableViewCell
        cell.setChart(values: dataStock[indexPath.row].valueStock)
        return cell
    }
}








