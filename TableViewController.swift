//
//  ViewController.swift
//  LineCharts
//
//  Created by Tran Son on 11/20/18.
//  Copyright © 2018 Tran Son. All rights reserved.
//

import UIKit

struct DataChart {
    var valueStock: [Double]
    var place: String!
    var province: String!
    var dollars: String!
    var percent: Double!
}

class TableViewController: UITableViewController {
    
    var dataStock = [DataChart]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataStock = [DataChart.init(valueStock: [20, 25, 35, 30], place: "TSLA", province: "Tesla, Inc", dollars: "$\(221.38)", percent: 6.52), DataChart.init(valueStock: [30, 25, 20, 35], place: "AAPL", province: "Apple, Inc", dollars: "$\(220.56)", percent: 5.2), DataChart.init(valueStock: [25, 35, 20, 15], place: "SQ", province: "Square", dollars: "$\(67.89)", percent: -3.42)]
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataStock.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Segue", for: indexPath) as! TableViewCell
        cell.setChart(values: dataStock[indexPath.row].valueStock, place: dataStock[indexPath.row].place, province: dataStock[indexPath.row].province, dollars: dataStock[indexPath.row].dollars, percent: dataStock[indexPath.row].percent)
        return cell
    }
    
  
}






